#!/bin/sh
CMD=$0
DIR_NAME=${PWD}
FILE_NAME=${CMD##*/}
EXCLUDE_REGEX='\(install.sh\)'
ShowHelp() {
	echo "${CMD}: please use command ./${FILE_NAME} [-a|--all]"
	exit 1
}
MakeLinks() {
	find * -regex "$EXCLUDE_REGEX" -prune -o -type f -print | 
		while read path; do MakeLink "$path"; done
}
MakeLink() {
	path=$*
	src=${DIR_NAME}/${path}
	dest=${HOME}/.${path}
	install -Dm644 "$src" "$dest" # install: create directory structure
	ln -fsv "$src" "$dest"
}
RootConfig() {
	echo; echo 'Installing system config files'

	# OVERWRITE() {
	echo; echo '*** /etc/default/unburden-home-dir ***'
	sudo tee /etc/default/unburden-home-dir <<EOF
UNBURDEN_HOME=true
EOF

	echo; echo '*** /etc/fonts/local.conf ***'
	sudo tee /etc/fonts/local.conf <<EOF
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
	<match target="font">
		<edit mode="assign" name="antialias">
			<bool>false</bool>
		</edit>
		<edit mode="assign" name="embeddedbitmap">
			<bool>false</bool>
		</edit>
		<edit mode="assign" name="hinting">
			<bool>true</bool>
		</edit>
		<edit mode="assign" name="hintstyle">
			<const>hintslight</const>
		</edit>
		<edit mode="assign" name="lcdfilter">
			<const>lcddefault</const>
		</edit>
		<edit mode="assign" name="rgba">
			<const>rgb</const>
		</edit>
	</match>
</fontconfig>
EOF

	echo; echo '*** /etc/vim/vimrc.local ***'
	sudo tee /etc/vim/vimrc.local <<EOF
set autochdir
set background=dark
set tags+=./tags;/
EOF

	echo; echo '*** /etc/X11/Xsession.d/91abc ***'
	sudo tee /etc/X11/Xsession.d/91abc <<EOF
export _JAVA_OPTIONS='-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dawt.useSystemAAFontSettings=off -Dswing.aatext=false'
export CHROMIUM_FLAGS='--disable-remote-fonts --disable-sync --no-default-browser-check'
setxkbmap -option ctrl:nocaps,ctrl:swap_lalt_lctl_lwin
xset m 0 0
EOF

	echo; echo '*** /etc/X11/xorg.conf.d/30-mouse-accel.conf ***'
	echo | sudo tee /etc/X11/xorg.conf.d/30-mouse-accel.conf
	# }

	# SYMLINK() {
	echo; echo '*** /etc/X11/xorg.conf.d/40-libinput.conf ***'
	sudo ln -fsv /usr/share/X11/xorg.conf.d/40-libinput.conf \
		/etc/X11/xorg.conf.d/40-libinput.conf
	# }
}
# Main() {
if [ "$CMD" != ./"$FILE_NAME" ]; then
	ShowHelp
fi
case $1 in
	-a|--all) MakeLinks; RootConfig;;
	"") MakeLinks;;
	*) ShowHelp;;
esac
# }
